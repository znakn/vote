<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Vote app</title>
</head>
<body>
<div class="row">
    <div class="col-1"></div>
    @foreach($politics as $key => $politic)
        <div class="col-1">


            <div class="card" style="width: 12rem;">
                <div class="card-body">
                    <h5 class="card-title">{{$politic}}</h5>
                    <p class="card-text">By interval : <span
                                id="polInterval{{$key}}">{{$data[$key]['count_interval']}}</span></p>
                    <p class="card-text">All : <span id="polAll{{$key}}">{{$data[$key]['count_all']}}</span></p>
                </div>
            </div>
        </div>
    @endforeach
    <div class="col-1"></div>
    <input type="hidden" id="intervalValue" value="{{env('DEFAULT_INTERVAL')}}">
    <input type="hidden" id="intervalId" value="">
    <input type="hidden" id="isRun" value="0">
</div>
<br>
<br>
<div class="row">
    <div class="col-2 offset-2">
        <button class="btn btn-success intButton" data-int-value="1000">Interval 1 sec</button>
    </div>
    <div class="col-2 offset-2">
        <button class="btn btn-success intButton" data-int-value="3000">Interval 3 sec</button>
    </div>
    <div class="col-2 offset-2">
        <button class="btn btn-success intButton" data-int-value="5000">Interval 5 sec</button>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-2 offset-3">
        <button class="btn btn-dark" id="startVote">Start emulate Vote</button>
    </div>
    <div class="col-2 offset-3">
        <button class="btn btn-danger" id="stopVote">Stop emulate Vote</button>
    </div>

</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script
        src="http://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>


<script>


    $(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function getData() {
            var isRun = $('#isRun').val();
            if (isRun != 0) {
                $.post('/get-data', {'interval': $('#intervalValue').val()}, function (res) {
                    $.each(res, function (k, v) {
                        $('#polInterval' + v.politic_id).html(v.count_interval);
                        $('#polAll' + v.politic_id).html(v.count_all);
                    });

                });
            }

        }

        function createInterval() {
            var oldIntervalId = parseInt($('#intervalId').val());
            var intervalValue = $('#intervalValue').val();
            console.log('Interval default ' + intervalValue);
            if (oldIntervalId) {
                console.log('clear old interval');
                clearInterval(oldIntervalId);
            }
            var newIntervalId = setInterval(getData, intervalValue);
            $('#intervalId').val(newIntervalId);
            console.log('start interval');
        }


        $(document).ready(function () {
            createInterval();
        });

        $('.intButton').click(function () {
            var intValue = $(this).data('int-value');
            console.log('click button interval ' + intValue);
            $('#intervalValue').val(intValue);
            createInterval();
        });

        $('#startVote').click(function () {
            $('#isRun').val(1);
            $.post('/start-vote', {}, function (res) {
                console.log(res);
            });
        });

        $('#stopVote').click(function () {
            $('#isRun').val(0);
            $.post('/stop-vote', {}, function (res) {
                console.log(res);
            });
        });

    });


</script>


</body>
</html>



