<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Settings;
use App\SmsInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Artisan;

class IndexController extends Controller
{
    public function index()
    {
        $politics = config('app.politics');


        $dateStart = date('Y-m-d H:i:s', time() - 3);
        $dateEnd = date('Y-m-d H:i:s');

        $data = SmsInfo::getDataByInterval($dateStart, $dateEnd);


        return view('index', ['politics' => $politics, 'data' => $data]);
    }

    public function startVote()
    {
        Settings::setValueByKey('run_vote', 1);
        Artisan::call('get-sms-info');
        return Response::json(['res' => 'Start vote']);
    }

    public function stopVote()
    {
        Settings::setValueByKey('run_vote', 100);
        return Response::json(['res' => 'Stop vote']);
    }

    public function getData(Request $request)
    {
        $interval = $request->post('interval', env('DEFAULT_INTERVAL'));
        $dateStart = date('Y-m-d H:i:s', time() - (int)($interval / 1000));
        $dateEnd = date('Y-m-d H:i:s');
        $data = SmsInfo::getDataByInterval($dateStart, $dateEnd);
        return $response = Response::json($data);
    }
}
