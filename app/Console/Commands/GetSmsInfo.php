<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Settings;

class GetSmsInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get-sms-info';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'In real project this command get info from SMS gateway. In this test we set information in sms-info table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        while (true)
        {
            $runVote = Settings::getValueByKey('run_vote');
            $this->info('Run session : '.$runVote);
            if ($runVote) {
                $this->makeVote();
            }
            if ($runVote == 100)
            {
                Settings::setValueByKey('run_vote',0);
                break;
            }
            sleep(1);
        }

        $this->info('Stop running command : '.$runVote);

    }


    private function makeVote()
    {
        $politics = config('app.politics');
        foreach ($politics as $key=>$value)
        {
            $insertInfo = [];
            $randomVoise = rand(1,20);
            for ($i=0;$i<$randomVoise;$i++)
            {
                $phoneNumber = rand(1000000,9999999);
                $insertInfo[] = [
                    'politic_id'=>$key,
                    'date_create'=>date('Y-m-d H:i:s'),
                    'phone_number'=>'+390'.$phoneNumber
                ];
            }
            DB::table('sms_info')->insert($insertInfo);
        }
    }
}
