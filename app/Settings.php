<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'value'];


    /**
     * @param $key
     * @return string
     */
    public static function getValueByKey($key)
    {
        $res = self::where(['key' => $key])->get();
        return isset($res[0]->value) ? $res[0]->value : '';
    }

    /**
     * @param $key
     * @param $value
     * @return Settings
     */
    public static function setValueByKey($key, $value)
    {
        $res = self::where(['key' => $key])->get();
        if (isset($res[0]->value)) {
            $setting = $res[0];
            $setting->value = $value;
        } else {
            $setting = new Settings();
            $setting->key = $key;
            $setting->value = $value;
        }
        $setting->save();
        return $setting;
    }
}
