<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SmsInfo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;


    public static function getDataByInterval($dateStart, $dateEnd)
    {

        $mas = [];
        $results = DB::select(DB::raw("SELECT i.politic_id,count(*) as count_all,(select count(*) from vote.sms_info i1 where i1.politic_id = i.politic_id and date_create BETWEEN :date_start and :date_end) as count_interval
         FROM vote.sms_info i GROUP By i.politic_id"), [
            'date_start' => $dateStart,
            'date_end' => $dateEnd
        ]);

        foreach ($results as $result) {
            $mas[$result->politic_id] = [
                'politic_id' => $result->politic_id,
                'count_all' => $result->count_all,
                'count_interval' => $result->count_interval
            ];
        }

        return $mas;
    }


}
